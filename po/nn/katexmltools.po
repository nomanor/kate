# Translation of katexmltools to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2002, 2004.
# Tor Hveem <tor@hveem.no>, 2004.
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2015, 2018.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: katexmltools\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2018-09-07 22:29+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: plugin_katexmltools.cpp:118
#, kde-format
msgid "XML Tools"
msgstr ""

#: plugin_katexmltools.cpp:121
#, kde-format
msgid "&Insert Element..."
msgstr "Set &inn element …"

#: plugin_katexmltools.cpp:126
#, kde-format
msgid "&Close Element"
msgstr "&Lukk element"

#: plugin_katexmltools.cpp:131
#, kde-format
msgid "Assign Meta &DTD..."
msgstr "Tildel meta-&DTD …"

#: plugin_katexmltools.cpp:442
#, kde-format
msgid "Assign Meta DTD in XML Format"
msgstr "Tildel meta-DTD i XML-format"

#: plugin_katexmltools.cpp:448
#, kde-format
msgid ""
"The current file has been identified as a document of type \"%1\". The meta "
"DTD for this document type will now be loaded."
msgstr ""
"Fila er identifisert til å vera av typen «%1». Lastar meta-DTD-en for denne "
"dokumenttypen."

#: plugin_katexmltools.cpp:452
#, kde-format
msgid "Loading XML Meta DTD"
msgstr "Opnar XML-meta-DTD"

#: plugin_katexmltools.cpp:484
#, kde-format
msgid "The file '%1' could not be opened. The server returned an error."
msgstr "Klarte ikkje opna fila «%1». Tenaren returnerte ein feil."

#: plugin_katexmltools.cpp:487 pseudo_dtd.cpp:48 pseudo_dtd.cpp:60
#, kde-format
msgid "XML Plugin Error"
msgstr "XML-modulfeil"

#: plugin_katexmltools.cpp:985
#, kde-format
msgid "XML entities"
msgstr "XML-entitetar"

#: plugin_katexmltools.cpp:987
#, kde-format
msgid "XML attribute values"
msgstr "XML-attributtverdiar"

#: plugin_katexmltools.cpp:989
#, kde-format
msgid "XML attributes"
msgstr "XML-attributt"

#: plugin_katexmltools.cpp:992
#, kde-format
msgid "XML elements"
msgstr "XML-element"

#: plugin_katexmltools.cpp:1031
#, kde-format
msgid "Insert XML Element"
msgstr "Set inn XML-element"

#: plugin_katexmltools.cpp:1036
#, kde-format
msgid ""
"Enter XML tag name and attributes (\"<\", \">\" and closing tag will be "
"supplied):"
msgstr ""
"Skriv inn XML-elementnamn og -attributt. «<», «>» og sluttaggar vert "
"automatisk sett inn:"

#: pseudo_dtd.cpp:45
#, kde-format
msgid ""
"The file '%1' could not be parsed. Please check that the file is well-formed "
"XML."
msgstr "Klarte ikkje tolka fila «%1». Kontroller at fila er velforma XML."

#: pseudo_dtd.cpp:54
#, kde-format
msgid ""
"The file '%1' is not in the expected format. Please check that the file is "
"of this type:\n"
"-//Norman Walsh//DTD DTDParse V2.0//EN\n"
"You can produce such files with dtdparse. See the Kate Plugin documentation "
"for more information."
msgstr ""
"Fila «%1» er ikkje i det forventa formatet. Kontroller at fila er av denne "
"typen:\n"
"-//Norman Walsh//DTD DTDParse V2.0//EN\n"
"Slike filer kan du laga med dtdparse. Hjelpeteksten til Kate-programtillegga "
"forklarar korleis."

#: pseudo_dtd.cpp:70
#, kde-format
msgid "Analyzing meta DTD..."
msgstr "Analyserer meta-DTD …"

#: pseudo_dtd.cpp:70
#, kde-format
msgid "Cancel"
msgstr "Avbryt"

#. i18n: ectx: Menu (xml)
#: ui.rc:6
#, kde-format
msgid "&XML"
msgstr "&XML"
