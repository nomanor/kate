# kategdbplugin.po translation el
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Stelios <sstavra@gmail.com>, 2012, 2013, 2015, 2020, 2023..
# Dimitrios Glentadakis <dglent@gmail.com>, 2012.
# Dimitris Kardarakos <dimkard@gmail.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kategdbplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-22 00:39+0000\n"
"PO-Revision-Date: 2023-01-02 10:17+0200\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, fuzzy, kde-format
#| msgid "GDB command"
msgid "GDB command"
msgstr "Εντολή GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Διαδρομές αναζήτησης αρχείων πηγής"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Τοπική εφαρμογή"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Απομακρυσμένο TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Απομακρυσμένη σειριακή θύρα"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Υπολογιστής"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Θύρα"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Προσαρμοσμένες εντολές εκκίνησης"

#: backend.cpp:24 backend.cpp:49 dapbackend.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Μια συνεδρία αποσφαλμάτωσης είναι σε εξέλιξη. Δώστε επανεκτέλεση ή διακόψτε "
"την τρέχουσα συνεδρία."

#: configview.cpp:93
#, kde-format
msgid "Add new target"
msgstr "Προσθήκη νέου προορισμού"

#: configview.cpp:97
#, kde-format
msgid "Copy target"
msgstr "Αντιγραφή προορισμού"

#: configview.cpp:101
#, kde-format
msgid "Delete target"
msgstr "Διαγραφή προορισμού"

#: configview.cpp:106
#, kde-format
msgid "Executable:"
msgstr "Εκτελέσιμο:"

#: configview.cpp:126
#, kde-format
msgid "Working Directory:"
msgstr "Κατάλογος εργασίας:"

#: configview.cpp:134
#, kde-format
msgid "Process Id:"
msgstr "Ταυτότητα διεργασίας:"

#: configview.cpp:139
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Ορίσματα:"

#: configview.cpp:142
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Διατήρηση εστίασης"

#: configview.cpp:143
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Να διατηρηθεί η εστίαση στη γραμμή εντολών"

#: configview.cpp:145
#, kde-format
msgid "Redirect IO"
msgstr "Ανακατεύθυνση εισόδου-εξόδου"

#: configview.cpp:146
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Ανακατεύθυνση των διορθωμένων προγραμμάτων IO σε ξεχωριστή καρτέλα"

#: configview.cpp:148
#, kde-format
msgid "Advanced Settings"
msgstr "Προηγμένες ρυθμίσεις"

#: configview.cpp:232
#, kde-format
msgid "Targets"
msgstr "Προορισμοί"

#: configview.cpp:525 configview.cpp:538
#, kde-format
msgid "Target %1"
msgstr "Προορισμός %1"

#: dapbackend.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "Αποτυχία συστήματος υποστήριξης DAP"

#: dapbackend.cpp:211
#, kde-format
msgid "program terminated"
msgstr "το πρόγραμμα τερματίστηκε"

#: dapbackend.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "ζητείται αποσύνδεση"

#: dapbackend.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "ζητείται τερματισμός"

#: dapbackend.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "Σύστημα υποστήριξης DAP: %1"

#: dapbackend.cpp:270 gdbbackend.cpp:653
#, kde-format
msgid "stopped (%1)."
msgstr "διακόπηκαν (%1)."

#: dapbackend.cpp:278 gdbbackend.cpp:657
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Ενεργό νήμα: %1 (όλα τα νήματα διακόπηκαν)."

#: dapbackend.cpp:280 gdbbackend.cpp:659
#, kde-format
msgid "Active thread: %1."
msgstr "Ενεργό νήμα: %1."

#: dapbackend.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Σημείο διακοπής εντοπίστηκε:"

#: dapbackend.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(σε συνέχεια) νήμα %1"

#: dapbackend.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "όλα τα νήματα σε συνέχεια"

#: dapbackend.cpp:316
#, kde-format
msgid "(running)"
msgstr "(εκτελείται)"

#: dapbackend.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** η σύνδεση με τον διακομιστή έκλεισε ***"

#: dapbackend.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "το πρόγραμμα τερματίστηκε με κωδικό %1"

#: dapbackend.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** αναμονή για τις ενέργειες του χρήστη ***"

#: dapbackend.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "σφάλμα στην απάντηση: %1"

#: dapbackend.cpp:445
#, kde-format
msgid "important"
msgstr "σημαντικό"

#: dapbackend.cpp:448
#, kde-format
msgid "telemetry"
msgstr "τηλεμετρία"

#: dapbackend.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "διαδικασία αποσφαλμάτωσης [%1] %2"

#: dapbackend.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "διαδικασία αποσφαλμάτωσης %1"

#: dapbackend.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Εκκίνηση μεθόδου: %1"

#: dapbackend.cpp:479
#, kde-format
msgid "thread %1"
msgstr "νήμα %1"

#: dapbackend.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "Σύνολο σημείων διακοπής"

#: dapbackend.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "Σημείο διακοπής καθαρό"

#: dapbackend.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) σημείο διακοπής"

#: dapbackend.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<not evaluated>"

#: dapbackend.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "δυνατότητες διακομιστή"

#: dapbackend.cpp:742
#, kde-format
msgid "supported"
msgstr "υποστηρίζεται"

#: dapbackend.cpp:742
#, kde-format
msgid "unsupported"
msgstr "δεν υποστηρίζεται"

#: dapbackend.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "Σημεία διακοπής υπό συνθήκη"

#: dapbackend.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "Σημεία διακοπής συνάρτησης"

#: dapbackend.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "στόχευση σημείων διακοπής υπό συνθήκη"

#: dapbackend.cpp:748
#, kde-format
msgid "log points"
msgstr "log points"

#: dapbackend.cpp:748
#, kde-format
msgid "modules request"
msgstr "αίτημα αρθρωμάτων"

#: dapbackend.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "αίτημα μετάβασης στους προορισμούς"

#: dapbackend.cpp:750
#, kde-format
msgid "terminate request"
msgstr "αίτημα τερματισμού"

#: dapbackend.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "τερματισμός προγράμματος σε αποσφαλμάτωση"

#: dapbackend.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "συντακτικό σφάλμα: η έκφραση δεν βρέθηκε"

#: dapbackend.cpp:976 dapbackend.cpp:1011 dapbackend.cpp:1049
#: dapbackend.cpp:1083 dapbackend.cpp:1119 dapbackend.cpp:1155
#: dapbackend.cpp:1191 dapbackend.cpp:1291 dapbackend.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "συντακτικό σφάλμα: %1"

#: dapbackend.cpp:984 dapbackend.cpp:1019 dapbackend.cpp:1298
#: dapbackend.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "μη έγκυρη γραμμή: %1"

#: dapbackend.cpp:991 dapbackend.cpp:996 dapbackend.cpp:1026
#: dapbackend.cpp:1031 dapbackend.cpp:1322 dapbackend.cpp:1327
#: dapbackend.cpp:1368 dapbackend.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "δεν ορίστηκε αρχείο: %1"

#: dapbackend.cpp:1061 dapbackend.cpp:1095 dapbackend.cpp:1131
#: dapbackend.cpp:1167 dapbackend.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "μη έγκυρη ταυτότητα νήματος: %1"

#: dapbackend.cpp:1067 dapbackend.cpp:1101 dapbackend.cpp:1137
#: dapbackend.cpp:1173 dapbackend.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "δεν ορίστηκε ταυτότητα νήματος: %1"

#: dapbackend.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Διαθέσιμες εντολές:"

#: dapbackend.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "Ο διακομιστής δεν υποστηρίζει σημεία διακοπής υπό συνθήκη"

#: dapbackend.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""
"η στόχευση των υπό συνθήκη σημείων διακοπής δεν υποστηρίζεται από τον "
"διακομιστή"

#: dapbackend.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr ""

#: dapbackend.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "δεν βρέθηκε σημείο διακοπής: (%1:%2)"

#: dapbackend.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Τρέχον νήμα: "

#: dapbackend.cpp:1392 dapbackend.cpp:1399 dapbackend.cpp:1423
#, kde-format
msgid "none"
msgstr "κανένα"

#: dapbackend.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Τρέχον πλαίσιο: "

#: dapbackend.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Κατάσταση συνεδρίας: "

#: dapbackend.cpp:1405
#, kde-format
msgid "initializing"
msgstr "γίνεται αρχικοποίηση"

#: dapbackend.cpp:1408
#, kde-format
msgid "running"
msgstr "εκτελείται"

#: dapbackend.cpp:1411
#, kde-format
msgid "stopped"
msgstr "διακόπηκε"

#: dapbackend.cpp:1414
#, kde-format
msgid "terminated"
msgstr "τερματίστηκε"

#: dapbackend.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "αποσυνδέθηκε"

#: dapbackend.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "νεκροψία"

#: dapbackend.cpp:1476
#, kde-format
msgid "command not found"
msgstr "η εντολή δεν βρέθηκε"

#: dapbackend.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "λείπει η ταυτότητα νήματος"

#: dapbackend.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "τερματισμός συστήματος υποστήριξης"

#: dapbackend.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Τρέχον πλαίσιο [%3]: %1:%2 (%4)"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "Settings"
msgid "Settings File:"
msgstr "Ρυθμίσεις"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr ""

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, fuzzy, kde-format
#| msgid "Debug"
msgid "Debugger"
msgstr "Διόρθωση"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: gdbbackend.cpp:35
#, kde-format
msgid "Locals"
msgstr "Τοπικά"

#: gdbbackend.cpp:37
#, kde-format
msgid "CPU registers"
msgstr ""

#: gdbbackend.cpp:158
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""

#: gdbbackend.cpp:167
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""

#: gdbbackend.cpp:176
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""

#: gdbbackend.cpp:382
#, kde-format
msgid "Could not start debugger process"
msgstr "Αδυναμία έναρξης διεργασίας διόρθωσης σφαλμάτων"

#: gdbbackend.cpp:440
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** το gdb τερματίστηκε κανονικά ***"

#: gdbbackend.cpp:646
#, kde-format
msgid "all threads running"
msgstr "όλα τα νήματα εκτελούνται"

#: gdbbackend.cpp:648
#, kde-format
msgid "thread(s) running: %1"
msgstr "Νήμα(τα) σε εκτέλεση: %1"

#: gdbbackend.cpp:678
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Τρέχον πλαίσιο: %1:%2"

#: gdbbackend.cpp:705
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Υπολογιστής: %1. Προορισμός: %1"

#: gdbbackend.cpp:1375
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Αδυναμία ανάλυσης της τελευταίας απάντησης: %1. Πάρα πολλά "
"συνεχόμενα σφάλματα. Γίνεται τερματισμός."

#: gdbbackend.cpp:1377
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Αδυναμία ανάλυσης της τελευταίας απάντησης: %1"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Σύμβολο"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Τιμή"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "τύπος"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "δεικτοδοτημένα αντικείμενα"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "αντικείμενα με όνομα"

#: plugin_kategdb.cpp:103
#, fuzzy, kde-format
#| msgid "Kate GDB"
msgid "Kate Debug"
msgstr "Kate GDB"

#: plugin_kategdb.cpp:107
#, kde-format
msgid "Debug View"
msgstr "Προβολή διόρθωσης σφαλμάτων"

#: plugin_kategdb.cpp:107 plugin_kategdb.cpp:340
#, kde-format
msgid "Debug"
msgstr "Διόρθωση"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:113
#, kde-format
msgid "Locals and Stack"
msgstr "Τοπικά και στοίβα"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Αρ"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Πλαίσιο"

#: plugin_kategdb.cpp:197
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "GDB Output"
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Έξοδος GDB"

#: plugin_kategdb.cpp:198
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Ρυθμίσεις"

#: plugin_kategdb.cpp:240
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Αδυναμία ανοίγματος αρχείου:</title><nl/>%1<br/>Δοκιμάστε να "
"προσθέσετε μια διαδρομή αναζήτησης στο Προηγμένες ρυθμίσεις -> Διαδρομές "
"αναζήτησης πηγαίων αρχείων"

#: plugin_kategdb.cpp:265
#, kde-format
msgid "Start Debugging"
msgstr "Έναρξη διόρθωσης σφαλμάτων"

#: plugin_kategdb.cpp:275
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Διακοπή διόρθωσης σφαλμάτων"

#: plugin_kategdb.cpp:282
#, kde-format
msgid "Continue"
msgstr "Συνέχεια"

#: plugin_kategdb.cpp:288
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Εναλλαγή σημείου διακοπής"

#: plugin_kategdb.cpp:294
#, kde-format
msgid "Step In"
msgstr "Είσοδος"

#: plugin_kategdb.cpp:301
#, kde-format
msgid "Step Over"
msgstr "Πέρασμα"

#: plugin_kategdb.cpp:308
#, kde-format
msgid "Step Out"
msgstr "Έξοδος"

#: plugin_kategdb.cpp:315 plugin_kategdb.cpp:347
#, kde-format
msgid "Run To Cursor"
msgstr "Εκτέλεση στο δρομέα"

#: plugin_kategdb.cpp:322
#, kde-format
msgid "Restart Debugging"
msgstr "Επανέναρξη διόρθωσης σφαλμάτων"

#: plugin_kategdb.cpp:330 plugin_kategdb.cpp:349
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Μετακίνηση μετρητή"

#: plugin_kategdb.cpp:335
#, kde-format
msgid "Print Value"
msgstr "Εμφάνιση τιμής"

#: plugin_kategdb.cpp:344
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:346
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:428 plugin_kategdb.cpp:444
#, kde-format
msgid "Insert breakpoint"
msgstr "Εισαγωγή σημείου διακοπής"

#: plugin_kategdb.cpp:442
#, kde-format
msgid "Remove breakpoint"
msgstr "Αφαίρεση σημείου διακοπής"

#: plugin_kategdb.cpp:571
#, kde-format
msgid "Execution point"
msgstr "Σημείο εκτέλεσης"

#: plugin_kategdb.cpp:710
#, kde-format
msgid "Thread %1"
msgstr "Νήμα %1"

#: plugin_kategdb.cpp:810
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:894
#, kde-format
msgid "Breakpoint"
msgstr "Σημείο διακοπής"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Διόρθωση"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, fuzzy, kde-format
#| msgid "GDB Plugin"
msgid "Debug Plugin"
msgstr "Πρόσθετο GDB"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stelios"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sstavra@gmail.com"

#~ msgid "GDB Integration"
#~ msgstr "Ολοκλήρωση GDB"

#~ msgid "Kate GDB Integration"
#~ msgstr "Ολοκλήρωση Kate GDB"

#~ msgid "/dev/ttyUSB0"
#~ msgstr "/dev/ttyUSB0"

#~ msgid "9600"
#~ msgstr "9600"

#~ msgid "14400"
#~ msgstr "14400"

#~ msgid "19200"
#~ msgstr "19200"

#~ msgid "38400"
#~ msgstr "38400"

#~ msgid "57600"
#~ msgstr "57600"

#~ msgid "115200"
#~ msgstr "115200"
