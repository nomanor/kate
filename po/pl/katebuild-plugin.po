# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maciej <maciekw5@gmail.com>, 2010.
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2010.
# SPDX-FileCopyrightText: 2011, 2012, 2014, 2015, 2018, 2019, 2020, 2021, 2022, 2023 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-17 01:37+0000\n"
"PO-Revision-Date: 2023-12-23 11:32+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmai.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Łukasz Wojniłowicz"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lukasz.wojnilowicz@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Dane wyjściowe"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Zbuduj ponownie"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Anuluj"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr "Dodaj błędy i ostrzeżenia do diagnostyki"

#: buildconfig.cpp:27
#, kde-format
msgid "Automatically switch to output pane on executing the selected target"
msgstr "Sam przełącz na wyjście tekstowe z programu przed jego wykonaniem"

#: buildconfig.cpp:40
#, kde-format
msgid "Build & Run"
msgstr "Zbuduj i uruchom"

#: buildconfig.cpp:46
#, kde-format
msgid "Build & Run Settings"
msgstr "Ustawienia budowania i uruchamiania"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1225
#, kde-format
msgid "Build"
msgstr "Buduj"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "Wybierz cel..."

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "Zbuduj wybrany cel"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "Zbuduj i uruchom wybrany cel"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Zatrzymaj"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "Przełącz na następną kartę po lewej"

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "Przełącz na następną kartę po lewej"

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Ustawienia celu"

#: plugin_katebuild.cpp:403
#, kde-format
msgid "Build Information"
msgstr "Informacja z budowy"

#: plugin_katebuild.cpp:620
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Nie wybrano pliku lub katalogu dla kompilacji."

#: plugin_katebuild.cpp:624
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Plik \"%1\" nie jest plikiem lokalnym. Kompilować można tylko pliki lokalne."

#: plugin_katebuild.cpp:674
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"Nie można wykonać polecenia: %1\n"
"Nie istnieje ścieżka pracy: %2"

#: plugin_katebuild.cpp:688
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Nie udało się uruchomić \"%1\". Wynik = %2"

#: plugin_katebuild.cpp:703
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Porzucono budowanie <b>%1</b>"

#: plugin_katebuild.cpp:810
#, kde-format
msgid "No target available for building."
msgstr "Brak celu do budowania."

#: plugin_katebuild.cpp:824
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "Nie wybrano lokalnego pliku lub katalogu dla budowania."

#: plugin_katebuild.cpp:830
#, kde-format
msgid "Already building..."
msgstr "Już w trakcie budowy..."

#: plugin_katebuild.cpp:857
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Budowanie celu <b>%1</b> ..."

#: plugin_katebuild.cpp:871
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Wyniki Make:</title><nl/>%1"

#: plugin_katebuild.cpp:907
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Ukończono budowanie <b>%1</b>. %2 błędy, %3 ostrzeżenia, %4 uwagi"

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Znaleziono jeden błąd."
msgstr[1] "Znaleziono %1 błędy."
msgstr[2] "Znaleziono %1 błędów."

#: plugin_katebuild.cpp:917
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Znaleziono jedno ostrzeżenie."
msgstr[1] "Znaleziono %1 ostrzeżenia."
msgstr[2] "Znaleziono %1 ostrzeżeń."

#: plugin_katebuild.cpp:920
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Znaleziono jedną uwagę."
msgstr[1] "Znaleziono %1 uwagi."
msgstr[2] "Znaleziono %1 uwag."

#: plugin_katebuild.cpp:925
#, kde-format
msgid "Build failed."
msgstr "Budowanie nieudane."

#: plugin_katebuild.cpp:927
#, kde-format
msgid "Build completed without problems."
msgstr "Budowanie zakończone bez problemów."

#: plugin_katebuild.cpp:932
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Zaniechano budowy <b>%1</b>. %2 błędy, %3 ostrzeżenia, %4 uwagi"

#: plugin_katebuild.cpp:956
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "Nie można wykonać: %1 Nie ustawiono katalogu pracy."

#: plugin_katebuild.cpp:1182
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "błąd"

#: plugin_katebuild.cpp:1185
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "ostrzeżenie"

#: plugin_katebuild.cpp:1188
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr "uwaga|info"

#: plugin_katebuild.cpp:1191
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "nieokreślone odniesienie"

#: plugin_katebuild.cpp:1224 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "Zestaw celów"

#: plugin_katebuild.cpp:1226
#, kde-format
msgid "Clean"
msgstr "Oczyść"

#: plugin_katebuild.cpp:1227
#, kde-format
msgid "Config"
msgstr "Ustawienia"

#: plugin_katebuild.cpp:1228
#, kde-format
msgid "ConfigClean"
msgstr "Oczyszczenie ustawień"

#: plugin_katebuild.cpp:1419
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr "Nie można zapisać celów do zbudowania w: %1"

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>T:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Dir:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Pozostaw puste, aby użyć katalogu bieżącego dokumentu.\n"
"Aby dodać katalog wyszukiwania, dodawaj ścieżki oddzielone znakiem ';'"

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Użycie:\n"
"\"%f\" dla bieżącego pliku\n"
"\"%d\" dla katalogu bieżącego pliku\n"
"\"%n\" dla bieżącej nazwy pliku bez przyrostka"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr "Projekt"

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr "Posiedzenie"

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "Nazwa polecenia/zestawu celów"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "Katalog roboczy / polecenie"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "Wykonaj polecenie"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""
"Odfiltruj cele, użyj klawiszy strzałek do zaznaczania i klawisza Enter do "
"wykonywania"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Utwórz nowy zestaw celów"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Skopiuj polecenie lub zestaw celów"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Usuń bieżący cel lub zestaw celów"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Dodaj nowy cel"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Zbuduj wybrany cel"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "Zbuduj i uruchom wybrany cel"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "Przesuń zaznaczony cel w górę"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "Przesuń zaznaczony cel w dół"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "Z&buduj"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Wstaw ścieżkę"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Wybierz katalog do wstawienia"

#~ msgid "Project Plugin Targets"
#~ msgstr "Cele wtyczki projektu"

#~ msgid "build"
#~ msgstr "buduj"

#~ msgid "clean"
#~ msgstr "oczyść"

#~ msgid "quick"
#~ msgstr "szybko"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Ukończono budowanie <b>%1</b>."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "Budowanie <b>%1</b> zawiera błędy."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Budowanie <b>%1</b> zawiera ostrzeżenia."

#~ msgid "Show:"
#~ msgstr "Pokaż:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Plik"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Linia"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Wiadomość"

#~ msgid "Next Error"
#~ msgstr "Następny błąd"

#~ msgid "Previous Error"
#~ msgstr "Poprzedni błąd"

#~ msgid "Show Marks"
#~ msgstr "Pokaż znaczniki"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>Nie można otworzyć pliku:</title><nl/>%1<br/>Spróbuj dodać ścieżkę "
#~ "wyszukiwania do katalogu pracy w ustawieniach celów"

#~ msgid "Error"
#~ msgstr "Błąd"

#~ msgid "Warning"
#~ msgstr "Ostrzeżenie"

#~ msgid "Only Errors"
#~ msgstr "Tylko błędy"

#~ msgid "Errors and Warnings"
#~ msgstr "Błędy i ostrzeżenia"

#~ msgid "Parsed Output"
#~ msgstr "Przetworzony wynik"

#~ msgid "Full Output"
#~ msgstr "Pełny wynik"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr "Zaznacz pole, aby uczynić to polecenie domyślnym dla zestawu celów."

#~ msgid "Select active target set"
#~ msgstr "Wybierz aktywny zestaw celów"

#~ msgid "Filter targets"
#~ msgstr "Odfiltruj cele"

#~ msgid "Build Default Target"
#~ msgstr "Zbuduj domyślny cel"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "Zbuduj domyślny cel"

#~ msgid "Build Previous Target"
#~ msgstr "Zbuduj poprzedni cel"

#~ msgid "Active target-set:"
#~ msgstr "Aktywny zestaw celów:"

#~ msgid "config"
#~ msgstr "ustawienia"

#~ msgid "Kate Build Plugin"
#~ msgstr "Wtyczka budowania Kate"

#~ msgid "Select build target"
#~ msgstr "Wybierz cel budowania"

#~ msgid "Filter"
#~ msgstr "Filtr"

#~ msgid "Build Output"
#~ msgstr "Wynik budowania"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "<title>Make Results:</title><nl/>%1"
#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Wyniki Make:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Następny zestaw celów"

#~ msgid "No previous target to build."
#~ msgstr "Brak poprzednich celów do zbudowania."

#~ msgid "No target set as default target."
#~ msgstr "Brak zestawu celów ustawionych jako domyślny cel."

#~ msgid "No target set as clean target."
#~ msgstr "Brak zestawu celów ustawionych jako czysty cel."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Nie znaleziono celu \"%1\" do budowania."

#~ msgid "Really delete target %1?"
#~ msgstr "Czy na pewno usunąć cel %1?"

#~ msgid "Nothing built yet."
#~ msgstr "Jeszcze nic nie zbudowano."

#~ msgid "Target Set %1"
#~ msgstr "Zestaw celów %1"

#~ msgid "Target"
#~ msgstr "Cel"

#~ msgid "Target:"
#~ msgstr "Cel:"

#~ msgid "from"
#~ msgstr "od"

#~ msgid "Sets of Targets"
#~ msgstr "Zestaw celów"

#~ msgid "Make Results"
#~ msgstr "Wyniki polecenia kompilacji"

#~ msgid "Others"
#~ msgstr "Inne"

#~ msgid "Quick Compile"
#~ msgstr "Szybka kompilacja"

#~ msgid "The custom command is empty."
#~ msgstr "Pole własnego polecenia jest puste."

#~ msgid "New"
#~ msgstr "Nowy"

#~ msgid "Copy"
#~ msgstr "Kopiuj"

#~ msgid "Delete"
#~ msgstr "Usuń"

#~ msgid "Quick compile"
#~ msgstr "Szybka kompilacja"

#~ msgid "Run make"
#~ msgstr "Uruchom make"

#~ msgid "Break"
#~ msgstr "Przerwij"
